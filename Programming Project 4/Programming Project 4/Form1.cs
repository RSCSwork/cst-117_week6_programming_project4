﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//This is my own work, Rikk Shimizu


/*
 * Some instructions for use, this is designed for two people to play.
 * Since X always starts, as soon as the application loads you can start play.
 * However you can always hit the new game button and get the prompt that X goes
 * first. From there you can play until the board is filled up or someone wins. 
 * You will be prompted by a message at the top for whose turn it is. 
 */


namespace Programming_Project_4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string[,] board = new string[3,3]; //this will be the game board for everything
        int turnCounter = 0;

        /*
         * the new game button starts the other methods that do the work. 
         */
        private void NewGameBtn_Click(object sender, EventArgs e)
        {
            resetBoard();
            turnCounter = 0;
            //Reset all the buttons to blanks.
            topLeftBoardBtn.Text = "";
            topCenterBoardBtn.Text = "";
            topRightBoardBtn.Text = "";

            middleLeftBoardBtn.Text = "";
            middleCenterBoardBtn.Text = "";
            middleRightBoardBtn.Text = "";

            bottomLeftBoardBtn.Text = "";
            bottomCenterBoardBtn.Text = "";
            bottomRightBoardBtn.Text = "";

            winnerTxtBx.Text = "";
            whoseTurnLbl.Text = "We start with Player X!";
        }//ends new game btn

        //Just exits the application
        private void ExitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }//end exit btn

        /*
         * reset board method will turn every value inside the array to an "a"
         * that way the check method will not get confused in between turns, but 
         * also allows for resets of the board between games.
         */
         public void resetBoard()
        {
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    board[i, j] = "a"; //set all values in the board to a to differentiate from X and O 
                }//ends inner for
            }//ends outer for
        }//ends resetBoard

        /*
         * This method will determine the winner by checking the board
         * for y and x wins or a potential draw.
         * 
         * Some notes on the logic, we were told to make player x and player y, I 
         * have assigned player x to X's and player y to O's. 
         * This can be reflected through player y winning when the boxes check out to "O", 
         * and player x winning when boxes check out to "X".
         */
        public void findWinner()
        {
            //using the basic checks on whether or not x and o have 3 in a row
            if (board[0, 0] == "O" && board[0, 1] == "O" && board[0, 2] == "O"
                || board[1, 0] == "O" && board[1, 1] == "O" && board[1, 2] == "O"
                || board[2, 0] == "O" && board[2, 1] == "O" && board[2, 2] == "O"
                || board[0, 0] == "O" && board[1, 0] == "O" && board[2, 0] == "O"
                || board[0, 1] == "O" && board[1, 1] == "O" && board[2, 1] == "O"
                || board[0, 2] == "O" && board[1, 2] == "O" && board[2, 2] == "O"
                || board[0, 0] == "O" && board[1, 1] == "O" && board[2, 2] == "O"
                || board[0, 2] == "O" && board[1, 1] == "O" && board[2, 0] == "O")
            {
                winnerTxtBx.Text = "Player Y won!";
            }
            if (board[0, 0] == "X" && board[0, 1] == "X" && board[0, 2] == "X"
                || board[1, 0] == "X" && board[1, 1] == "X" && board[1, 2] == "X"
                || board[2, 0] == "X" && board[2, 1] == "X" && board[2, 2] == "X"
                || board[0, 0] == "X" && board[1, 0] == "X" && board[2, 0] == "X"
                || board[0, 1] == "X" && board[1, 1] == "X" && board[2, 1] == "X"
                || board[0, 2] == "X" && board[1, 2] == "X" && board[2, 2] == "X"
                || board[0, 0] == "X" && board[1, 1] == "X" && board[2, 2] == "X"
                || board[0, 2] == "X" && board[1, 1] == "X" && board[2, 0] == "X")
            {
                winnerTxtBx.Text = "Player X won!";
            }//ends if else

            //if moves 0-8 have happened, the board must be full so it must be a draw now.
            if(turnCounter == 8)
            {
                winnerTxtBx.Text = "It was a draw!";
            }
        }//ends find winner

        /* each button click will change the text of the button based on 
        * whose turn it is on the counter, and fill a space in the 2d 
        * array based on the position on the board. It will also update the 
        * Gui and add to the counter. 
        */
        private void TopLeftBoardBtn_Click(object sender, EventArgs e)
        {
            if(turnCounter%2 == 0)
            {
                topLeftBoardBtn.Text = "X";
                board[0, 0] = "X";
                whoseTurnLbl.Text = "It is now Player Y's turn.";
                findWinner();
                turnCounter++;
            }
            else
            {
                topLeftBoardBtn.Text = "O";
                board[0, 0] = "O";
                whoseTurnLbl.Text = "It is now Player X's turn.";
                findWinner();
                turnCounter++;
            }//ends if else
        }//ends top left button

        private void TopCenterBoardBtn_Click(object sender, EventArgs e)
        {
            if (turnCounter % 2 == 0)
            {
                topCenterBoardBtn.Text = "X";
                board[0, 1] = "X";
                whoseTurnLbl.Text = "It is now Player Y's turn.";
                findWinner();
                turnCounter++;
            }
            else
            {
                topCenterBoardBtn.Text = "O";
                board[0, 1] = "O";
                whoseTurnLbl.Text = "It is now Player X's turn.";
                findWinner();
                turnCounter++;
            }//ends if else
        }//ends top center

        private void TopRightBoardBtn_Click(object sender, EventArgs e)
        {
            if (turnCounter % 2 == 0)
            {
                topRightBoardBtn.Text = "X";
                board[0, 2] = "X";
                whoseTurnLbl.Text = "It is now Player Y's turn.";
                findWinner();
                turnCounter++;
            }
            else
            {
                topRightBoardBtn.Text = "O";
                board[0, 2] = "O";
                whoseTurnLbl.Text = "It is now Player X's turn.";
                findWinner();
                turnCounter++;
            }//ends if else
        }//ends top right

        private void MiddleLeftBoardBtn_Click(object sender, EventArgs e)
        {
            if (turnCounter % 2 == 0)
            {
                middleLeftBoardBtn.Text = "X";
                board[1, 0] = "X";
                whoseTurnLbl.Text = "It is now Player Y's turn.";
                findWinner();
                turnCounter++;
            }
            else
            {
                middleLeftBoardBtn.Text = "O";
                board[1, 0] = "O";
                whoseTurnLbl.Text = "It is now Player X's turn.";
                findWinner();
                turnCounter++;
            }//ends if else
        }//ends middle left

        private void MiddleCenterBoardBtn_Click(object sender, EventArgs e)
        {
            if (turnCounter % 2 == 0)
            {
                middleCenterBoardBtn.Text = "X";
                board[1, 1] = "X";
                whoseTurnLbl.Text = "It is now Player Y's turn.";
                findWinner();
                turnCounter++;
            }
            else
            {
                middleCenterBoardBtn.Text = "O";
                board[1, 1] = "O";
                whoseTurnLbl.Text = "It is now Player X's turn.";
                findWinner();
                turnCounter++;
            }//ends if else
        }//ends middle center

        private void MiddleRightBoardBtn_Click(object sender, EventArgs e)
        {
            if (turnCounter % 2 == 0)
            {
                middleRightBoardBtn.Text = "X";
                board[1, 2] = "X";
                whoseTurnLbl.Text = "It is now Player Y's turn.";
                findWinner();
                turnCounter++;
            }
            else
            {
                middleRightBoardBtn.Text = "O";
                board[1, 2] = "O";
                whoseTurnLbl.Text = "It is now Player X's turn.";
                findWinner();
                turnCounter++;
            }//ends if else
        }//ends middle right

        private void BottomLeftBoardBtn_Click(object sender, EventArgs e)
        {
            if (turnCounter % 2 == 0)
            {
                bottomLeftBoardBtn.Text = "X";
                board[2, 0] = "X";
                whoseTurnLbl.Text = "It is now Player Y's turn.";
                findWinner();
                turnCounter++;
            }
            else
            {
                bottomLeftBoardBtn.Text = "O";
                board[2, 0] = "O";
                whoseTurnLbl.Text = "It is now Player X's turn.";
                findWinner();
                turnCounter++;
            }//ends if else
        }//ends bottom left

        private void BottomCenterBoardBtn_Click(object sender, EventArgs e)
        {
            if (turnCounter % 2 == 0)
            {
                bottomCenterBoardBtn.Text = "X";
                board[2, 1] = "X";
                whoseTurnLbl.Text = "It is now Player Y's turn.";
                findWinner();
                turnCounter++;
            }
            else
            {
                bottomCenterBoardBtn.Text = "O";
                board[2, 1] = "O";
                whoseTurnLbl.Text = "It is now Player X's turn.";
                findWinner();
                turnCounter++;
            }//ends if else
        }//ends bottom center

        private void BottomRightBoardBtn_Click(object sender, EventArgs e)
        {
            if (turnCounter % 2 == 0)
            {
                bottomRightBoardBtn.Text = "X";
                board[2, 2] = "X";
                whoseTurnLbl.Text = "It is now Player Y's turn.";
                findWinner();
                turnCounter++;
            }
            else
            {
                bottomRightBoardBtn.Text = "O";
                board[2, 2] = "O";
                whoseTurnLbl.Text = "It is now Player X's turn.";
                findWinner();
                turnCounter++;
            }//ends if else
        }//ends bottom right

    }//ends partial class
}//ends namespace
