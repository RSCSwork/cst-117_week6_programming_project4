﻿namespace Programming_Project_4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.winnerTxtBx = new System.Windows.Forms.TextBox();
            this.newGameBtn = new System.Windows.Forms.Button();
            this.exitBtn = new System.Windows.Forms.Button();
            this.whoseTurnLbl = new System.Windows.Forms.Label();
            this.topLeftBoardBtn = new System.Windows.Forms.Button();
            this.topCenterBoardBtn = new System.Windows.Forms.Button();
            this.topRightBoardBtn = new System.Windows.Forms.Button();
            this.middleLeftBoardBtn = new System.Windows.Forms.Button();
            this.middleCenterBoardBtn = new System.Windows.Forms.Button();
            this.middleRightBoardBtn = new System.Windows.Forms.Button();
            this.bottomLeftBoardBtn = new System.Windows.Forms.Button();
            this.bottomCenterBoardBtn = new System.Windows.Forms.Button();
            this.bottomRightBoardBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // winnerTxtBx
            // 
            this.winnerTxtBx.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.winnerTxtBx.Location = new System.Drawing.Point(115, 380);
            this.winnerTxtBx.MinimumSize = new System.Drawing.Size(200, 50);
            this.winnerTxtBx.Name = "winnerTxtBx";
            this.winnerTxtBx.Size = new System.Drawing.Size(237, 44);
            this.winnerTxtBx.TabIndex = 9;
            // 
            // newGameBtn
            // 
            this.newGameBtn.Location = new System.Drawing.Point(115, 465);
            this.newGameBtn.MinimumSize = new System.Drawing.Size(100, 75);
            this.newGameBtn.Name = "newGameBtn";
            this.newGameBtn.Size = new System.Drawing.Size(100, 75);
            this.newGameBtn.TabIndex = 10;
            this.newGameBtn.Text = "New Game!";
            this.newGameBtn.UseVisualStyleBackColor = true;
            this.newGameBtn.Click += new System.EventHandler(this.NewGameBtn_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(252, 465);
            this.exitBtn.MinimumSize = new System.Drawing.Size(100, 75);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(100, 75);
            this.exitBtn.TabIndex = 11;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // whoseTurnLbl
            // 
            this.whoseTurnLbl.AutoSize = true;
            this.whoseTurnLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.whoseTurnLbl.Location = new System.Drawing.Point(110, 39);
            this.whoseTurnLbl.Name = "whoseTurnLbl";
            this.whoseTurnLbl.Size = new System.Drawing.Size(0, 25);
            this.whoseTurnLbl.TabIndex = 12;
            // 
            // topLeftBoardBtn
            // 
            this.topLeftBoardBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.topLeftBoardBtn.Location = new System.Drawing.Point(115, 116);
            this.topLeftBoardBtn.MaximumSize = new System.Drawing.Size(75, 75);
            this.topLeftBoardBtn.MinimumSize = new System.Drawing.Size(75, 75);
            this.topLeftBoardBtn.Name = "topLeftBoardBtn";
            this.topLeftBoardBtn.Size = new System.Drawing.Size(75, 75);
            this.topLeftBoardBtn.TabIndex = 13;
            this.topLeftBoardBtn.UseVisualStyleBackColor = true;
            this.topLeftBoardBtn.Click += new System.EventHandler(this.TopLeftBoardBtn_Click);
            // 
            // topCenterBoardBtn
            // 
            this.topCenterBoardBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.topCenterBoardBtn.Location = new System.Drawing.Point(196, 116);
            this.topCenterBoardBtn.MaximumSize = new System.Drawing.Size(75, 75);
            this.topCenterBoardBtn.MinimumSize = new System.Drawing.Size(75, 75);
            this.topCenterBoardBtn.Name = "topCenterBoardBtn";
            this.topCenterBoardBtn.Size = new System.Drawing.Size(75, 75);
            this.topCenterBoardBtn.TabIndex = 14;
            this.topCenterBoardBtn.UseVisualStyleBackColor = true;
            this.topCenterBoardBtn.Click += new System.EventHandler(this.TopCenterBoardBtn_Click);
            // 
            // topRightBoardBtn
            // 
            this.topRightBoardBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.topRightBoardBtn.Location = new System.Drawing.Point(277, 116);
            this.topRightBoardBtn.MaximumSize = new System.Drawing.Size(75, 75);
            this.topRightBoardBtn.MinimumSize = new System.Drawing.Size(75, 75);
            this.topRightBoardBtn.Name = "topRightBoardBtn";
            this.topRightBoardBtn.Size = new System.Drawing.Size(75, 75);
            this.topRightBoardBtn.TabIndex = 15;
            this.topRightBoardBtn.UseVisualStyleBackColor = true;
            this.topRightBoardBtn.Click += new System.EventHandler(this.TopRightBoardBtn_Click);
            // 
            // middleLeftBoardBtn
            // 
            this.middleLeftBoardBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.middleLeftBoardBtn.Location = new System.Drawing.Point(115, 197);
            this.middleLeftBoardBtn.MaximumSize = new System.Drawing.Size(75, 75);
            this.middleLeftBoardBtn.MinimumSize = new System.Drawing.Size(75, 75);
            this.middleLeftBoardBtn.Name = "middleLeftBoardBtn";
            this.middleLeftBoardBtn.Size = new System.Drawing.Size(75, 75);
            this.middleLeftBoardBtn.TabIndex = 16;
            this.middleLeftBoardBtn.UseVisualStyleBackColor = true;
            this.middleLeftBoardBtn.Click += new System.EventHandler(this.MiddleLeftBoardBtn_Click);
            // 
            // middleCenterBoardBtn
            // 
            this.middleCenterBoardBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.middleCenterBoardBtn.Location = new System.Drawing.Point(196, 197);
            this.middleCenterBoardBtn.MaximumSize = new System.Drawing.Size(75, 75);
            this.middleCenterBoardBtn.MinimumSize = new System.Drawing.Size(75, 75);
            this.middleCenterBoardBtn.Name = "middleCenterBoardBtn";
            this.middleCenterBoardBtn.Size = new System.Drawing.Size(75, 75);
            this.middleCenterBoardBtn.TabIndex = 17;
            this.middleCenterBoardBtn.UseVisualStyleBackColor = true;
            this.middleCenterBoardBtn.Click += new System.EventHandler(this.MiddleCenterBoardBtn_Click);
            // 
            // middleRightBoardBtn
            // 
            this.middleRightBoardBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.middleRightBoardBtn.Location = new System.Drawing.Point(277, 197);
            this.middleRightBoardBtn.MaximumSize = new System.Drawing.Size(75, 75);
            this.middleRightBoardBtn.MinimumSize = new System.Drawing.Size(75, 75);
            this.middleRightBoardBtn.Name = "middleRightBoardBtn";
            this.middleRightBoardBtn.Size = new System.Drawing.Size(75, 75);
            this.middleRightBoardBtn.TabIndex = 18;
            this.middleRightBoardBtn.UseVisualStyleBackColor = true;
            this.middleRightBoardBtn.Click += new System.EventHandler(this.MiddleRightBoardBtn_Click);
            // 
            // bottomLeftBoardBtn
            // 
            this.bottomLeftBoardBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bottomLeftBoardBtn.Location = new System.Drawing.Point(115, 278);
            this.bottomLeftBoardBtn.MaximumSize = new System.Drawing.Size(75, 75);
            this.bottomLeftBoardBtn.MinimumSize = new System.Drawing.Size(75, 75);
            this.bottomLeftBoardBtn.Name = "bottomLeftBoardBtn";
            this.bottomLeftBoardBtn.Size = new System.Drawing.Size(75, 75);
            this.bottomLeftBoardBtn.TabIndex = 19;
            this.bottomLeftBoardBtn.UseVisualStyleBackColor = true;
            this.bottomLeftBoardBtn.Click += new System.EventHandler(this.BottomLeftBoardBtn_Click);
            // 
            // bottomCenterBoardBtn
            // 
            this.bottomCenterBoardBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bottomCenterBoardBtn.Location = new System.Drawing.Point(196, 278);
            this.bottomCenterBoardBtn.MaximumSize = new System.Drawing.Size(75, 75);
            this.bottomCenterBoardBtn.MinimumSize = new System.Drawing.Size(75, 75);
            this.bottomCenterBoardBtn.Name = "bottomCenterBoardBtn";
            this.bottomCenterBoardBtn.Size = new System.Drawing.Size(75, 75);
            this.bottomCenterBoardBtn.TabIndex = 20;
            this.bottomCenterBoardBtn.UseVisualStyleBackColor = true;
            this.bottomCenterBoardBtn.Click += new System.EventHandler(this.BottomCenterBoardBtn_Click);
            // 
            // bottomRightBoardBtn
            // 
            this.bottomRightBoardBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bottomRightBoardBtn.Location = new System.Drawing.Point(277, 278);
            this.bottomRightBoardBtn.MaximumSize = new System.Drawing.Size(75, 75);
            this.bottomRightBoardBtn.MinimumSize = new System.Drawing.Size(75, 75);
            this.bottomRightBoardBtn.Name = "bottomRightBoardBtn";
            this.bottomRightBoardBtn.Size = new System.Drawing.Size(75, 75);
            this.bottomRightBoardBtn.TabIndex = 21;
            this.bottomRightBoardBtn.UseVisualStyleBackColor = true;
            this.bottomRightBoardBtn.Click += new System.EventHandler(this.BottomRightBoardBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 596);
            this.Controls.Add(this.bottomRightBoardBtn);
            this.Controls.Add(this.bottomCenterBoardBtn);
            this.Controls.Add(this.bottomLeftBoardBtn);
            this.Controls.Add(this.middleRightBoardBtn);
            this.Controls.Add(this.middleCenterBoardBtn);
            this.Controls.Add(this.middleLeftBoardBtn);
            this.Controls.Add(this.topRightBoardBtn);
            this.Controls.Add(this.topCenterBoardBtn);
            this.Controls.Add(this.topLeftBoardBtn);
            this.Controls.Add(this.whoseTurnLbl);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.newGameBtn);
            this.Controls.Add(this.winnerTxtBx);
            this.Name = "Form1";
            this.Text = "Tic Tac Toe";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox winnerTxtBx;
        private System.Windows.Forms.Button newGameBtn;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Label whoseTurnLbl;
        private System.Windows.Forms.Button topLeftBoardBtn;
        private System.Windows.Forms.Button topCenterBoardBtn;
        private System.Windows.Forms.Button topRightBoardBtn;
        private System.Windows.Forms.Button middleLeftBoardBtn;
        private System.Windows.Forms.Button middleCenterBoardBtn;
        private System.Windows.Forms.Button middleRightBoardBtn;
        private System.Windows.Forms.Button bottomLeftBoardBtn;
        private System.Windows.Forms.Button bottomCenterBoardBtn;
        private System.Windows.Forms.Button bottomRightBoardBtn;
    }
}

